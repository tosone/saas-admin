# FusionCloud: an OpenStack SDK for Go

FusionCloud is a flexible SDK that allows you to consume and work with OpenStack
clouds in a simple and idiomatic way using golang. Many services are supported,
including Compute, Block Storage, Object Storage, Networking, and Identity.
Each service API is backed with getting started guides, code samples, reference
documentation, unit tests and acceptance tests.

### FusionCloud useful image list

|image name|id|
|:---|:---|
|CentOS 7.2 64位|297466d0-08ae-4b6b-998a-023add13a805|
|CentOS 7.5 64位|02a64260-0373-4ac7-9e03-95e7848c6968|
|Ubuntu 16.04 服务器版 64位|08c8a881-f95b-46c4-b10c-e3eaa836376f|
|Debian 9.4.2|1f872071-cfc4-4d56-b059-fa3d8c8c82ad|
|CentOS 6.8 64位|a8d89658-a1f9-4c23-9a16-4865289582c2|
|CentOS 7.4 32位|994eb542-0cc2-454d-8c68-b619a2db4659|
|CentOS 6.9 64位|7f27f093-a336-4c9d-875f-6fe33a5c3afc|
|SUSE 12 企业版 SP1 64位|9805591b-dd36-46b9-a50c-96079e84e1c8|
|CentOS 7.4 64位|cfb3eb6a-9613-495a-8ce5-70c9b2e4d35f|
|Redhat 7.4 企业版 64位|edd3bb60-4c47-4f5c-ae2a-c5ddd366dee4|

### FusionCloud useful flavor list

|CPU|MEM|ID|
|:---|:---|:---|
|1|2|db.c2.medium|
|1|4|db.s1.medium|
|1|8|db.m1.medium|
|2|4|db.c2.large|
|2|8|db.s1.large|
|2|16|db.m1.large|
|4|8|db.c2.xlarge|
|4|16|db.s1.xlarge|
|4|32|db.m1.xlarge|
|8|8|rds.c1.2xlarge|
|8|16|db.c2.2xlarge|
|8|32|db.s1.2xlarge|
|16|64|db.s1.4xlarge|
|16|128|db.m1.4xlarge|
|32|128|db.s1.8xlarge|
