package volumes

import "github.com/caicloud/cluster-admin/pkg/util/fusioncloud"

func createURL(c *fusioncloud.ServiceClient) string {
	return c.ServiceURL("volumes")
}

func listURL(c *fusioncloud.ServiceClient) string {
	return c.ServiceURL("volumes", "detail")
}

func deleteURL(c *fusioncloud.ServiceClient, id string) string {
	return c.ServiceURL("volumes", id)
}

func getURL(c *fusioncloud.ServiceClient, id string) string {
	return deleteURL(c, id)
}

func updateURL(c *fusioncloud.ServiceClient, id string) string {
	return deleteURL(c, id)
}
