package tokens

import "github.com/caicloud/cluster-admin/pkg/util/fusioncloud"

func tokenURL(c *fusioncloud.ServiceClient) string {
	return c.ServiceURL("auth", "tokens")
}
