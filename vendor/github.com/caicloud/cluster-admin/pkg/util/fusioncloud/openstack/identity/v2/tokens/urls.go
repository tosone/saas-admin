package tokens

import "github.com/caicloud/cluster-admin/pkg/util/fusioncloud"

// CreateURL generates the URL used to create new Tokens.
func CreateURL(client *fusioncloud.ServiceClient) string {
	return client.ServiceURL("tokens")
}

// GetURL generates the URL used to Validate Tokens.
func GetURL(client *fusioncloud.ServiceClient, token string) string {
	return client.ServiceURL("tokens", token)
}
