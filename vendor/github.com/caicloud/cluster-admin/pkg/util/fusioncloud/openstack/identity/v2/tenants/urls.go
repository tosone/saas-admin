package tenants

import "github.com/caicloud/cluster-admin/pkg/util/fusioncloud"

func listURL(client *fusioncloud.ServiceClient) string {
	return client.ServiceURL("tenants")
}
