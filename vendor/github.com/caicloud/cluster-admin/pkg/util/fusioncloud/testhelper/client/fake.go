package client

import (
	"github.com/caicloud/cluster-admin/pkg/util/fusioncloud"
	"github.com/caicloud/cluster-admin/pkg/util/fusioncloud/testhelper"
)

// Fake token to use.
const TokenID = "cbc36478b0bd8e67e89469c7749d4127"

// ServiceClient returns a generic service client for use in tests.
func ServiceClient() *fusioncloud.ServiceClient {
	return &fusioncloud.ServiceClient{
		ProviderClient: &fusioncloud.ProviderClient{TokenID: TokenID},
		Endpoint:       testhelper.Endpoint(),
	}
}
