module gitlab.com/tosone

go 1.13

require (
	github.com/caicloud/clientset v0.0.0-20190919062857-b21562ce589e
	github.com/caicloud/cluster-admin v1.6.2-0.20191120024818-feaba729f3db
	github.com/caicloud/nirvana v0.2.2-0.20190505014918-75eea4ec013f
	github.com/go-logr/zapr v0.1.1 // indirect
	github.com/googleapis/gnostic v0.3.0 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/magiconair/properties v1.8.1 // indirect
	github.com/onsi/ginkgo v1.10.2 // indirect
	github.com/onsi/gomega v1.7.0 // indirect
	github.com/pelletier/go-toml v1.4.0 // indirect
	github.com/prometheus/alertmanager v0.17.0
	github.com/prometheus/client_golang v0.9.4 // indirect
	github.com/shurcooL/httpfs v0.0.0-20190527155220-6a4d4a70508b // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd // indirect
	github.com/spf13/cobra v0.0.5 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/viper v1.4.0 // indirect
	github.com/tidwall/gjson v1.3.2
	github.com/tidwall/sjson v1.0.4
	golang.org/x/net v0.0.0-20190724013045-ca1201d0de80 // indirect
	golang.org/x/oauth2 v0.0.0-20190604053449-0f29369cfe45 // indirect
	golang.org/x/sys v0.0.0-20190610081024-1e42afee0f76 // indirect
	google.golang.org/appengine v1.6.1 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	k8s.io/api v0.0.0-20190516230258-a675ac48af67
	k8s.io/apimachinery v0.0.0-20190404173353-6a84e37a896d
	k8s.io/client-go v11.0.1-0.20190516230509-ae8359b20417+incompatible
	k8s.io/klog v1.0.0 // indirect
	k8s.io/utils v0.0.0-20191010214722-8d271d903fe4 // indirect
	sigs.k8s.io/controller-runtime v0.1.12
	sigs.k8s.io/yaml v1.1.0 // indirect
)
