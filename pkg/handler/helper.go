package handler

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	v1 "gitlab.com/tosone/saas-admin/pkg/types/v1"
	amtmpl "github.com/prometheus/alertmanager/template"
)

const (
	alertPodNameAnno      = "compass_alert_target_pod"
	alertResourceKindAnno = "compass_alert_target_kind"
	alertThresholdAnno    = "compass_alert_threshold"
	alertValueAnno        = "compass_alert_value"
	alertUnitAnno         = "compass_alert_unit"
)

// getAlertAnnotation return the annotation of alert
func getAlertAnnotation(v amtmpl.Alert, k string) string {
	return v.Annotations[k]
}

// getLabel return the label of alert
func getLabel(v amtmpl.Alert, k string) string {
	return v.Labels[k]
}

func getThreshold(msg v1.AlertData) string {
	threshold := msg.GetAnnotation(v1.AlertThresholdAnno)
	unit := msg.GetAnnotation(v1.AlertUnitAnno)
	cs := fmt.Sprintf("%s %s", threshold, unit)
	return cutStringNumber(strings.TrimSpace(cs))
}

func cutStringNumber(s string) string {
	s1 := strings.Split(s, " ")
	var value, unit string
	value = s
	if len(s1) == 2 {
		value, unit = s1[0], s1[1]
	}
	valueF, err := strconv.ParseFloat(value, 64)
	if err != nil {
		fmt.Println(err)
	} else {
		value = fmt.Sprintf("%f", valueF)
	}
	if unit == "B" { //convert Byte to Mib or Gib....
		value, unit = GetValueAndFormat(ByteSize(valueF))
	}
	ss := strings.Split(value, ".")
	if len(ss) != 2 {
		return s
	}
	if len(ss[1]) > 5 {
		if strings.Replace(ss[1][:5], "0", "", -1) != "" {
			value = fmt.Sprintf("%s.%s", ss[0], ss[1][:5])
		} else {
			value = ss[0]
		}
	}

	return fmt.Sprintf("%s%s", value, unit)
}

type ByteSize float64

const (
	B   ByteSize = 1
	KiB          = 1024 * B
	MiB          = 1024 * KiB
	GiB          = 1024 * MiB
	TiB          = 1024 * GiB
	PiB          = 1024 * TiB
)

func GetValueAndFormat(b ByteSize) (value string, format string) {
	if int64(b/KiB) == 0 {
		return fmt.Sprintf("%f", b), "B"
	} else if int64(b/KiB) > 0 && int64(b/MiB) == 0 {
		return fmt.Sprintf("%f", b/KiB), "KiB"
	} else if int64(b/MiB) > 0 && int64(b/GiB) == 0 {
		return fmt.Sprintf("%f", b/MiB), "MiB"
	} else if int64(b/GiB) > 0 && int64(b/TiB) == 0 {
		return fmt.Sprintf("%f", b/GiB), "GiB"
	} else if int64(b/TiB) > 0 && int64(b/PiB) == 0 {
		return fmt.Sprintf("%f", b/TiB), "TiB"
	}
	return fmt.Sprintf("%f", b/PiB), "PiB"
}

func convertToAlarm(as *v1.AlertData, productID string) v1.AlarmResp {
	now := time.Now()
	v := v1.AlarmResp{
		InstanceID: productID,
		Level:      2,
		PolicyInfo: fmt.Sprintf("%s %s %s",
			as.GetAnnotation(v1.AlertMetricNameAnno),
			as.GetAnnotation(v1.AlertOprationAnno),
			getThreshold(*as)),
		SupplierID: os.Getenv("SUPPLIER_ID"),
		TimeStamp:  now.Unix(),
		Time:       as.Alerts[0].StartsAt.Unix(),
	}
	level := as.GetAnnotation(v1.AlertLevelAnno)
	if level == "1" {
		v.Level = 3
	}
	detail := v1.AlertDetailResp{
		Title:       as.GetAnnotation(v1.AlertMetricNameAnno),
		MonitorType: v1.AlertType(as.GetAnnotation(v1.AlertResourceKindAnno)),
	}
	for _, a := range as.Alerts {
		d := v1.AlertInfo{
			Tag:   "",
			Time:  a.StartsAt.Unix(),
			Unit:  a.Annotations[v1.AlertUnitAnno],
			Value: a.Annotations[v1.AlertValueAnno],
		}
		targetName := a.Annotations[v1.AlertPodAnno]
		if targetName == "" {
			targetName = a.Annotations[v1.AlertReleaseAnno]
		}
		if targetName != "" {
			d.Tag = fmt.Sprintf("%s/%s", a.Annotations[v1.AlertNamespaceAnno], targetName)
		}
		switch v1.AlertType(a.Annotations[v1.AlertResourceKindAnno]) {
		case v1.PartitionAlertKind:
			if d.Tag == "" {
				d.Tag = a.Annotations[v1.AlertNamespaceAnno]
			}
		case v1.NodeAlertKind:
			if d.Tag == "" {
				d.Tag = a.Annotations[v1.AlertNodeAnno]
			}
		default:
			if d.Tag == "" {
				d.Tag = a.Annotations[v1.AlertClusterAnno]
			}
		}
		detail.Items = append(detail.Items, d)
	}

	v.Detail = detail
	return v
}
