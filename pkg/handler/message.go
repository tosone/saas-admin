package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"os"

	v1 "gitlab.com/tosone/saas-admin/pkg/types/v1"
	"github.com/caicloud/nirvana/log"

	"gitlab.com/tosone/saas-admin/pkg/sender"
	"gitlab.com/tosone/saas-admin/pkg/utils"

	meta_v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

//SendMessage send message to socket
func SendMessage(ctx context.Context, msg *v1.AlertData) error {
	// generate kubernetes client
	if len(msg.Alerts) <= 0 {
		return nil
	}
	cid := msg.GetAnnotation(v1.AlertClusterAnno)
	clusterInfo, err := utils.GetInitClient().ResourceV1beta1().Clusters().Get(cid, meta_v1.GetOptions{})
	if err != nil {
		log.Errorln(err)
		return nil
	}
	productID := clusterInfo.GetAnnotations()[utils.ClusterProductIDAnno]
	// convert alert data
	alarm := convertToAlarm(msg, productID)
	reqData, err := json.Marshal(alarm)
	if err != nil {
		log.Errorln(err)
		return err
	}
	// push alert
	path := fmt.Sprintf("%s%s", os.Getenv("REMOTE_ALERT_SERVER"), "/opc/monitor/alarm")
	supplierID := os.Getenv("SUPPLIER_ID")
	childCtx := context.WithValue(
		context.WithValue(ctx, sender.URLCtx, path),
		sender.SupplierKey, supplierID)

	var resp v1.CommonResp
	err = sender.Send(childCtx, reqData, resp)
	if err != nil {
		log.Errorln("push alert error:", err)
		return err
	}
	if !resp.Success {
		return fmt.Errorf("code:%s,msg:%s", resp.Code, resp.Desc)
	}
	return nil
}
