package handler

import (
	"context"
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"net/url"
	"os"
	"sort"
	"strconv"
	"time"

	"github.com/caicloud/nirvana/log"
	"github.com/caicloud/nirvana/service"

	"gitlab.com/tosone/saas-admin/pkg/sender"
	"gitlab.com/tosone/saas-admin/pkg/types/saasv1"
	"gitlab.com/tosone/saas-admin/pkg/utils"
)

// CleverMsg handle a notify message
func CleverMsg(ctx context.Context, query saasv1.NotifyQuery) (interface{}, error) {
	log.Infoln(query)

	if err := verifySign(ctx); err != nil {
		return nil, err
	}
	// validate query
	if err := checkCommonParam(query); err != nil {
		log.Errorln(err)
		return nil, err
	}
	switch query.Action {
	case utils.SaasActionCreate:
		return handleCreateCleverTenant(query)
	case utils.SaasActionUpdate:
		err := handleUpdateCleverTenant(query)
		if err != nil {
			return nil, err
		}
		return saasv1.SaasResp{Success: "true"}, nil
	case utils.SaasActionDelete:
		err := handleDeleteCleverTenant(query)
		if err != nil {
			return nil, err
		}
		return saasv1.SaasResp{Success: "true"}, nil
	default:
		return saasv1.SaasResp{Success: "true"}, nil
	}
}

func checkCommonParam(query saasv1.NotifyQuery) error {
	if query.Action != utils.SaasActionCreate &&
		query.Action != utils.SaasActionUpdate &&
		query.Action != utils.SaasActionDelete &&
		query.Action != utils.SaasActionExpire &&
		query.Action != utils.SaasActionRenew {
		return fmt.Errorf("undefined action:%s", query.Action)
	}

	now := time.Now().Unix()
	timestamp, err := strconv.ParseInt(query.Timestamp, 10, 64)
	if err != nil {
		return err
	}
	if now-60 > timestamp {
		return nil // fmt.Errorf("timeout,now:%d,timestamp:%d", now, timestamp)
	}
	return nil
}

func handleCreateCleverTenant(query saasv1.NotifyQuery) (*saasv1.SaasCreateCleverResp, error) {
	cauthServer := os.Getenv("CAUTH_SERVER")
	if cauthServer == "" {
		cauthServer = "http://dex-cauth:8080"
	}
	tenantServer := os.Getenv("TENANT_SERVER")
	if tenantServer == "" {
		tenantServer = "http://tenant-admin:8080"
	}

	cleverServer := os.Getenv("CLEVER_SERVER")
	if cleverServer == "" {
		cleverServer = "http://10.5.36.10:6060/"
	}
	defaultPasswd := os.Getenv("DEFAULT_PASSWD")
	if len(defaultPasswd) == 0 {
		defaultPasswd = "12345678"
	}

	productInfo := saasv1.SaasTenantProductInfo{}
	err := json.Unmarshal([]byte(query.ProductInfo), &productInfo)
	if err != nil {
		return nil, err
	}

	var fnvHash = fnv.New32()
	if _, err := fnvHash.Write([]byte(strconv.FormatInt(time.Now().Unix(), 10))); err != nil {
		return nil, err
	}
	var username = hex.EncodeToString(fnvHash.Sum(nil)) + "2" // generate a hex string as tenant name
	// create user
	err = sender.CreateUser(cauthServer, username, query.Email, defaultPasswd)
	if err != nil {
		log.Errorln(err)
		return nil, err
	}

	// create tenant
	tenantID, err := sender.CreateTenant(cauthServer, username)
	if err != nil {
		log.Errorln(err)
		return nil, err
	}

	// bind cluster
	err = sender.AllocateTenantQuota(tenantServer, tenantID, productInfo)
	if err != nil {
		log.Errorln(err)
		return nil, err
	}

	resp := &saasv1.SaasCreateCleverResp{}
	resp.InstanceID = query.InstanceID
	resp.AppInfo.Website = cleverServer
	resp.AppInfo.AdminURL = cleverServer
	resp.AppInfo.Username = username
	resp.AppInfo.Password = defaultPasswd
	resp.Success = "true"

	return resp, nil
}

func handleUpdateCleverTenant(query saasv1.NotifyQuery) error {
	tenantServer := os.Getenv("TENANT_SERVER")
	if tenantServer == "" {
		tenantServer = "http://tenant-admin:8080"
	}

	if query.InstanceID == "" {
		return fmt.Errorf("empty InstanceID")
	}

	productInfo := saasv1.SaasTenantProductInfo{}
	err := json.Unmarshal([]byte(query.ProductInfo), &productInfo)
	if err != nil {
		return err
	}

	return sender.UpdateTenantQuota(tenantServer, query.InstanceID, productInfo)
}

func handleDeleteCleverTenant(query saasv1.NotifyQuery) error {
	cauthServer := os.Getenv("CAUTH_SERVER")
	if cauthServer == "" {
		cauthServer = "http://dex-cauth:8080"
	}
	tenantServer := os.Getenv("TENANT_SERVER")
	if tenantServer == "" {
		tenantServer = "http://tenant-admin:8080"
	}

	// delete user
	err := sender.DeleteTenantQuota(tenantServer, query.InstanceID)
	if err != nil {
		log.Errorln(err)
		return err
	}

	// delete tenant
	err = sender.DeleteTenant(cauthServer, query.InstanceID)
	if err != nil {
		log.Errorln(err)
		return err
	}
	return nil
}

func verifySign(ctx context.Context) error {
	req := service.HTTPContextFrom(ctx).Request()
	urlValues := req.URL.Query()
	token := urlValues.Get("token")

	signData := getSignData(urlValues)
	signature := md5.Sum([]byte(signData))
	sign := fmt.Sprintf("%x", signature)

	if sign != token {
		log.Infof("sign:%s,token:%s,signData:%s\n", sign, token, signData)
		//return fmt.Errorf("signature error")
	}
	return nil
}

func getSignData(urlValues url.Values) string {
	md5Key := os.Getenv("MD5_KEY")
	if md5Key == "" {
		md5Key = "isvkey"
	}
	urlValues.Del("token")
	signString := ""
	type value struct {
		key    string
		values []string
	}

	keySlice := make([]value, len(urlValues))
	i := 0
	for key, values := range urlValues {
		keySlice[i] = value{key: key, values: values}
		i++
	}
	sort.Slice(keySlice, func(i int, j int) bool {
		return keySlice[i].key < keySlice[j].key
	})
	for _, v := range keySlice {
		for _, vv := range v.values {
			if len(vv) == 0 {
				continue
			}
			signString += fmt.Sprintf("%s=%s&", v.key, vv)
		}
	}

	signString += "key=" + md5Key
	return signString
}
