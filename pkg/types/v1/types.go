package v1

import (
	amtmpl "github.com/prometheus/alertmanager/template"
)

var (
	resourceKindMap = map[string]string{
		"application":  "应用",
		"partition":    "分区",
		"node":         "节点",
		"cluster":      "集群",
		"loadbalancer": "负载均衡",
	}
	metricMap = map[string]string{
		"metrics.app.total_cpu_usage":                              "应用CPU用量",
		"metrics.app.cpu_usage":                                    "容器CPU用量",
		"metrics.app.total_memory_usage":                           "应用内存用量",
		"metrics.app.memory_usage":                                 "容器内存用量",
		"metrics.app.total_gpu_memory_usage":                       "应用显存利用率",
		"metrics.app.total_gpu_usage":                              "应用GPU利用率",
		"metrics.app.container_fs_usage":                           "容器硬盘用量",
		"metrics.app.total_fs_usage":                               "应用硬盘用量",
		"metrics.app.pod_network_in":                               "容器组网络接收率",
		"metrics.app.pod_network_out":                              "容器组网络发送率",
		"metrics.app.total_network_in":                             "应用网络接收率",
		"metrics.app.total_network_out":                            "应用网络发送率",
		"metrics.istio.total_requests_rate":                        "Istio 服务总请求率",
		"metrics.istio.error_requests_rate":                        "Istio 服务错误请求率",
		"metrics.istio.success_requests_rate":                      "Istio 服务成功请求率",
		"metrics.partition.cpu_usage":                              "分区CPU用量",
		"metrics.partition.memory_usage":                           "分区内存用量",
		"metrics.partition.fs_usage":                               "分区硬盘用量",
		"metrics.partition.network_in":                             "分区网络接收率",
		"metrics.partition.network_out":                            "分区网络发送率",
		"metrics.partition.pod_count":                              "分区容器数",
		"metrics.cluster.cpu_utilized":                             "集群CPU利用率",
		"metrics.cluster.memory_utilized":                          "集群内存利用率",
		"metrics.cluster.disk_read":                                "集群硬盘读取率",
		"metrics.cluster.disk_write":                               "集群硬盘写入率",
		"metrics.cluster.network_inbound":                          "集群网络接收率",
		"metrics.cluster.network_outbound":                         "集群网络发送率",
		"metrics.cluster.gpu_utilized":                             "集群GPU利用率",
		"metrics.cluster.gpu_memory_utilized":                      "集群显存利用率",
		"metrics.cluster.component.apiserver.cpu":                  "Kube API server CPU 用量",
		"metrics.cluster.component.apiserver.memory":               "Kube API server 内存用量",
		"metrics.cluster.component.apiserver.request_per_second":   "Kube API server 每秒请求量",
		"metrics.cluster.component.apiserver.p95latency":           "Kube API server 延迟 - 95百分位",
		"metrics.cluster.component.canal.cpu":                      "Canal CPU 用量",
		"metrics.cluster.component.canal.memory":                   "Canal 内存用量",
		"metrics.cluster.component.etcd-cluster.cpu":               "ETCD (cluster) CPU 用量",
		"metrics.cluster.component.etcd-cluster.memory":            "ETCD (cluster) 内存用量",
		"metrics.cluster.component.etcd-cluster.disk_sync":         "ETCD (cluster) Fsync 耗时",
		"metrics.cluster.component.etcd-event.cpu":                 "ETCD (event) CPU 用量",
		"metrics.cluster.component.etcd-event.memory":              "ETCD (event) 内存用量",
		"metrics.cluster.component.etcd-event.disk_sync":           "ETCD (event) Fsync 耗时",
		"metrics.cluster.component.kube-controller-manager.cpu":    "Kube controller manager CPU 用量",
		"metrics.cluster.component.kube-controller-manager.memory": "Kube controller manager 内存用量",
		"metrics.cluster.component.kube-proxy.cpu":                 "Kube proxy CPU 用量",
		"metrics.cluster.component.kube-proxy.memory":              "Kube proxy 内存用量",
		"metrics.cluster.component.kube-scheduler.cpu":             "Kube scheduler CPU 用量",
		"metrics.cluster.component.kube-scheduler.memory":          "Kube scheduler 内存用量",
		"metrics.cluster.component.mongodb.member_health":          "系统 MongoDB 成员健康状态",
		"metrics.cluster.resources.node.cpu_used_top":              "集群节点CPU用量前五",
		"metrics.cluster.resources.node.mem_used_top":              "集群节点内存用量前五",
		"metrics.cluster.resources.node.fs_used_top":               "集群节点硬盘用量前五",
		"metrics.cluster.resources.node.disk_read_top":             "集群节点硬盘读取率前五",
		"metrics.cluster.resources.node.disk_write_top":            "集群节点硬盘写入率前五",
		"metrics.cluster.resources.node.network_inbound_top":       "集群节点网络接受率前五",
		"metrics.cluster.resources.node.network_outbound_top":      "集群节点网络发送率前五",
		"metrics.cluster.resources.partition.pod_count":            "集群节点容器组数量前五",
		"metrics.cluster.resources.partition.cpu_used":             "集群分区CPU用量前五",
		"metrics.cluster.resources.partition.mem_used":             "集群分区内存用量前五",
		"metrics.event.system.other_warning_rate":                  "系统非Pod资源异常事件10分钟产生率",
		"metrics.event.system.pod_warning_rate":                    "系统Pod资源异常事件10分钟产生率",
		"metrics.event.system.app_warning_rate":                    "租户服务异常事件10分钟产生率",
		"metrics.event.system.volume_warning_rate":                 "租户数据卷异常事件10分钟产生率",
		"metrics.node.cpu_usage":                                   "节点CPU用量",
		"metrics.node.load_1m":                                     "节点1分钟平均CPU负载",
		"metrics.node.load_5m":                                     "节点5分钟平均CPU负载",
		"metrics.node.load_15m":                                    "节点15分钟平均CPU负载",
		"metrics.node.memory_active":                               "节点内存使用量",
		"metrics.node.memory_inactive":                             "节点闲置内存大小",
		"metrics.node.memory_buffers":                              "节点内存缓冲区大小",
		"metrics.node.memory_cached":                               "节点内存缓存区大小",
		"metrics.node.memory_free":                                 "节点可用内存总量",
		"metrics.node.gpu_mem_utilize":                             "节点显存利用率",
		"metrics.node.gpu_utilize":                                 "节点GPU利用率",
		"metrics.node.disk_read_by_device":                         "节点硬盘读取率",
		"metrics.node.disk_write_by_device":                        "节点硬盘写入率",
		"metrics.node.filesystem_free":                             "节点硬盘空闲大小",
		"metrics.node.filesystem_usage":                            "节点硬盘使用大小",
		"metrics.node.network_in":                                  "节点网络接收率",
		"metrics.node.network_out":                                 "节点网络发送率",
		"metrics.node.pod_cpu_usage":                               "节点容器组CPU用量",
		"metrics.node.pod_cpu_usage_top5":                          "节点容器组CPU用量前五",
		"metrics.node.pod_memory_usage":                            "节点容器组内存用量",
		"metrics.node.pod_memory_usage_top5":                       "节点容器组内存用量前五",
		"metrics.node.pod_network_in":                              "节点容器组网络接受量",
		"metrics.node.pod_network_in_top5":                         "节点容器组网络接受量前五",
		"metrics.node.pod_network_out":                             "节点容器组网络发送量",
		"metrics.node.pod_network_out_top5":                        "节点容器组网络发送量前五",
		"metrics.node.pod_count":                                   "节点容器数",
	}
	operationMap = map[string]string{
		">": "大于",
		"<": "小于",
		"=": "等于",
	}
)

const (
	AlertRuleAliasAnno    = "compass_alert_rule_alias"
	AlertResourceKindAnno = "compass_alert_target_kind"
	AlertClusterNameAnno  = "compass_alert_target_cluster"
	AlertMetricNameAnno   = "compass_alert_comparable"
	AlertOprationAnno     = "compass_alert_comparator"
	AlertPodNameAnno      = "compass_alert_target_pod"
	AlertThresholdAnno    = "compass_alert_threshold"
	AlertValueAnno        = "compass_alert_value"
	AlertUnitAnno         = "compass_alert_unit"
	AlertLevelAnno        = "compass_alert_level"
	AlertNamespaceAnno    = "compass_alert_target_namespace"
	AlertNodeAnno         = "compass_alert_target_node"
	AlertPodAnno          = "compass_alert_target_pod"
	AlertClusterAnno      = "compass_alert_target_cluster"
	AlertReleaseAnno      = "compass_alert_target_release"
)

type AlertData struct {
	*amtmpl.Data
	Location int
}

// GetAlertName return alert rule's name
func (t AlertData) GetAlertName() string {
	for _, alert := range t.Alerts {
		for k, v := range alert.Annotations {
			if k == AlertRuleAliasAnno {
				return v
			}
		}
	}
	return ""
}

// GetKind return alert rule's kind
func (t AlertData) GetKind() string {
	for _, alert := range t.Alerts {
		for k, v := range alert.Annotations {
			if k == AlertResourceKindAnno && v != "" {
				return resourceKindMap[v]
			}
		}
	}
	return "日志报警"
}

// GetClusterName return alert rule's cluster name
func (t AlertData) GetClusterName() string {
	for _, alert := range t.Alerts {
		for k, v := range alert.Annotations {
			if k == AlertClusterNameAnno && v != "" {
				return v
			}
		}
	}
	return ""
}

// GetMetricName return alert Metric
func (t AlertData) GetMetricName() string {
	for _, alert := range t.Alerts {
		for k, v := range alert.Annotations {
			if k == AlertMetricNameAnno && v != "" {
				return metricMap[v]
			}
		}
	}
	return ""
}

// GetOpetationName return alert operation
func (t AlertData) GetOpetationName() string {
	for _, alert := range t.Alerts {
		for k, v := range alert.Annotations {
			if k == AlertOprationAnno && v != "" {
				return operationMap[v]
			}
		}
	}
	return ""
}

// GetAnnotation return alert operation
func (t AlertData) GetAnnotation(a string) string {
	for _, alert := range t.Alerts {
		for k, v := range alert.Annotations {
			if k == a {
				return v
			}
		}
	}
	return ""
}

type AlertType string

const (
	ApplicationAlertKind  AlertType = "Release"
	PartitionAlertKind    AlertType = "Namespace"
	NodeAlertKind         AlertType = "Node"
	ClusterAlertKind      AlertType = "Cluster"
	LoadbalancerAlertKind AlertType = "Loadbalancer"
)

type AlertInfo struct {
	Tag   string `json:"tag,omitempty"`
	Time  int64  `json:"time,omitempty"`
	Unit  string `json:"unit,omitempty"`
	Value string `json:"value,omitempty"`
}

type AlertDetailResp struct {
	Items []AlertInfo `json:"items,omitempty"`
	// monitor Type e.g. application
	MonitorType AlertType `json:"monitorType,omitempty"`
	// alert name e.g. cpu使用率
	Title string `json:"title,omitempty"`
}

type AlarmResp struct {
	Detail     AlertDetailResp `json:"detail,omitempty"`
	InstanceID string          `json:"instanceId,omitempty"`
	Level      int             `json:"level,omitempty"`
	Message    string          `json:"message,omitempty"`
	PolicyInfo string          `json:"policyInfo,omitempty"`
	Sign       string          `json:"sign,omitempty"`
	SupplierID string          `json:"supplierId,omitempty"`
	Time       int64           `json:"time,omitempty"`
	TimeStamp  int64           `json:"timeStamp,omitempty"`
}

type CommonResp struct {
	Success bool        `json:"success"`
	Code    string      `json:"code"`
	Desc    string      `json:"desc"`
	Result  interface{} `json:"result"`
}

// huawei

type ErrorResponse struct {
	Message string `json:"message"`
	Code    string `josn:"code"`
}

type EvsVolumeList struct {
	Count        int         `json:"count"`
	Volumes      []EvsVolume `json:"volumes"`
	VolumesLinks []struct {
		Href string `json:"href"`
		Rel  string `json:"rel"`
	} `json:"volumes_links"`
	Error *ErrorResponse `json:"error,omitempty"`
}

type EvsVolumeStatus string

const (
	EvsVolumeCreating      EvsVolumeStatus = "creating"
	EvsVolumeAvailable     EvsVolumeStatus = "available"
	EvsVolumeInuse         EvsVolumeStatus = "in-use"
	EvsVolumeError         EvsVolumeStatus = "error"
	EvsVolumeAttaching     EvsVolumeStatus = "attaching"
	EvsVolumeDetaching     EvsVolumeStatus = "detaching"
	EvsVolumeDeleting      EvsVolumeStatus = "deleting"
	EvsVolumeErrorDeleting EvsVolumeStatus = "error_deleting"
)

type EvsVolume struct {
	Attachments []struct {
		ServerID     string `json:"server_id"`
		AttachmentID string `json:"attachment_id"`
		AttachedAt   string `json:"attached_at"`
		HostName     string `json:"host_name"`
		VolumeID     string `json:"volume_id"`
		Device       string `json:"device"`
		ID           string `json:"id"`
	} `json:"attachments"`
	AvailabilityZone   string `json:"availability_zone"`
	Bootable           string `json:"bootable"`
	ConsistencygroupID string `json:"consistencygroup_id"`
	CreatedAt          string `json:"created_at"`
	Description        string `json:"description"`
	Encrypted          bool   `json:"encrypted"`
	ID                 string `json:"id"`
	Links              []struct {
		Href string `json:"href"`
		Rel  string `json:"rel"`
	} `json:"links"`
	Metadata struct {
		SystemEncrypted string `json:"__system__encrypted,omitempty"`
		SystemCmkid     string `json:"__system__cmkid,omitempty"`
		HwPassthrough   string `json:"hw:passthrough,omitempty"`
	} `json:"metadata"`
	Name                    string          `json:"name"`
	OsVolHostAttrHost       string          `json:"os-vol-host-attr:host"`
	OsVolTenantAttrTenantID string          `json:"os-vol-tenant-attr:tenant_id"`
	ReplicationStatus       string          `json:"replication_status"`
	Multiattach             bool            `json:"multiattach"`
	Size                    int64           `json:"size"`
	SnapshotID              string          `json:"snapshot_id"`
	SourceVolid             string          `json:"source_volid"`
	Status                  EvsVolumeStatus `json:"status"`
	UpdatedAt               string          `json:"updated_at"`
	UserID                  string          `json:"user_id"`
	VolumeType              string          `json:"volume_type"`
	ServiceType             string          `json:"service_type"`
	DedicatedStorageID      string          `json:"dedicated_storage_id"`
	DedicatedStorageName    string          `json:"dedicated_storage_name"`
	Wwn                     string          `json:"wwn"`
}
