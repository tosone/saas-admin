package saasv1

// NotifyQuery respresent a notify message
type NotifyQuery struct {
	Action      string `source:"query,action"`
	AccountID   string `source:"query,accountId"`
	OrderID     string `source:"query,orderId"`
	ProductID   string `source:"query,productId"`
	RequestID   string `source:"query,requestId"`
	Timestamp   string `source:"query,timestamp"`
	ProductInfo string `source:"query,productInfo"`
	Email       string `source:"query,email"`
	InstanceID  string `source:"query,instanceId"`
}

// SaasResp respresent a saas response
type SaasResp struct {
	Success string `json:"success,omitempty"`
}

// SaasCreateCleverResp respresent a create response
type SaasCreateCleverResp struct {
	InstanceID string `json:"instanceId"`
	AppInfo    struct {
		Website  string `json:"website"`
		AdminURL string `json:"adminUrl"`
		Username string `json:"username"`
		Password string `json:"password"`
	} `json:"appInfo"`
	Success string `json:"success,omitempty"`
	// additionalInfo
}

// SaasTenantProductInfo respresent a product info
type SaasTenantProductInfo struct {
	TenantAlias   string `json:"tenantAlias"`
	StorageSize   int `json:"storageSize"`
	StorageNum    int `json:"storageNum"`
	GpuNum        int `json:"gpuNum"`
	CPURequest    int `json:"cpuRequest"`
	CPULimit      int `json:"cpuLimit"`
	MemoryRequest int `json:"memoryRequest"`
	MemoryLimit   int `json:"memoryLimit"`
}

// type name struct {

// }
