// +nirvana:api=descriptors:"Descriptor"

package apis

import (
	def "github.com/caicloud/nirvana/definition"

	"gitlab.com/tosone/saas-admin/pkg/apis/middlewares"
	saasv1 "gitlab.com/tosone/saas-admin/pkg/apis/saasv1/descriptors"
	v1 "gitlab.com/tosone/saas-admin/pkg/apis/v1/descriptors"
)

// Descriptor returns a combined descriptor for APIs of all versions.
func Descriptor() def.Descriptor {
	return def.Descriptor{
		Description: "APIs",
		Path:        "/apis",
		Middlewares: middlewares.Middlewares(),
		Consumes:    []string{def.MIMEJSON},
		Produces:    []string{def.MIMEJSON},
		Children: []def.Descriptor{
			v1.Descriptor(),
		},
	}
}

// SaasDescriptor returns a combined descriptor for APIs of all versions.
func SaasDescriptor() def.Descriptor {
	return def.Descriptor{
		Description: "APIs",
		Path:        "/apis",
		Middlewares: middlewares.Middlewares(),
		Consumes:    []string{def.MIMEJSON},
		Produces:    []string{def.MIMEJSON},
		Children: []def.Descriptor{
			saasv1.Descriptor(),
		},
	}
}
