package descriptors

import (
	def "github.com/caicloud/nirvana/definition"

	saas "gitlab.com/tosone/saas-admin/pkg/handler"
)

func init() {
	register([]def.Descriptor{
		{
			Path:        "/clever",
			Definitions: []def.Definition{receiveCleverMsg},
		},
	}...)
}

var receiveCleverMsg = def.Definition{
	Method:      def.Get,
	Summary:     "receive saas message",
	Description: "receive saas message",
	Function:    saas.CleverMsg,
	Parameters: []def.Parameter{
		def.AutoParameterFor("query"),
	},
	Results: def.DataErrorResults(""),
}
