package middlewares

import (
	def "github.com/caicloud/nirvana/definition"
)

// Middlewares returns a list of middlewares.
func Middlewares() []def.Middleware {
	return []def.Middleware{
		// verifySign,
	}
}

// func verifySign(ctx context.Context, chain def.Chain) error {

// 	req := service.HTTPContextFrom(ctx).Request()
// 	urlValues := req.URL.Query()
// 	token := urlValues.Get("token")

// 	fmt.Println(urlValues)
// 	fmt.Println(url.QueryUnescape(req.URL.RawQuery))

// 	signData := getSignData(urlValues)
// 	signature := md5.Sum([]byte(signData))
// 	sign := fmt.Sprintf("%x", signature)

// 	if sign != token {
// 		fmt.Printf("sign:%s,token:%s,signData:%s\n", sign, token, signData)
// 		return fmt.Errorf("signature error")
// 	}
// 	return chain.Continue(ctx)

// }

// func getSignData(urlValues url.Values) string {
// 	md5Key := os.Getenv("MD5_KEY")
// 	if md5Key == "" {
// 		md5Key = "isvkey"
// 	}
// 	urlValues.Del("token")
// 	signString := ""
// 	type value struct {
// 		key    string
// 		values []string
// 	}

// 	keySlice := make([]value, len(urlValues))
// 	i := 0
// 	for key, values := range urlValues {
// 		keySlice[i] = value{key: key, values: values}
// 		i++
// 	}
// 	sort.Slice(keySlice, func(i int, j int) bool {
// 		return keySlice[i].key < keySlice[j].key
// 	})
// 	for _, v := range keySlice {
// 		for _, vv := range v.values {
// 			signString += fmt.Sprintf("%s=%s&", v.key, vv)
// 		}
// 	}

// 	signString += "key=" + md5Key
// 	return signString
// }
