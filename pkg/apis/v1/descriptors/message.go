package descriptors

import (
	def "github.com/caicloud/nirvana/definition"

	message "gitlab.com/tosone/saas-admin/pkg/handler"
)

func init() {
	register([]def.Descriptor{{
		Path:        "/alerts",
		Definitions: []def.Definition{createAlerts},
	},
	}...)
}

var createAlerts = def.Definition{
	Method:      def.Create,
	Summary:     "send alerts",
	Description: "send alerts to socket",
	Function:    message.SendMessage,
	Parameters: []def.Parameter{
		def.BodyParameterFor(""),
	},
	Results: []def.Result{{
		Destination: def.Error,
	}},
}
