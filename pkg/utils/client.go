package utils

import (
	"context"
	"fmt"
	"os"

	"github.com/caicloud/clientset/kubernetes"
	"github.com/caicloud/nirvana/log"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client/config"

	"github.com/caicloud/cluster-admin/pkg/util/fusioncloud"
)

var (
	client kubernetes.Interface
)

func init() {
	// init cluster client
	cf, err := config.GetConfig()
	if err != nil {
		log.Errorln(err)
		panic(err)
	}
	client, err = kubernetes.NewForConfig(cf)
	if err != nil {
		log.Errorln(err)
		panic(err)
	}
	// region := os.Getenv("REGION")
}

// InitFusioncloudOpts get fusioncloud's config from env
func InitFusioncloudOpts() fusioncloud.AuthOptions {
	opts := fusioncloud.AuthOptions{}
	if iamEndpoint := os.Getenv("IDENTITY_ENDPOINT"); iamEndpoint == "" {
		opts.IdentityEndpoint = "https://iam-apigateway-proxy.peopledailycloud.com/v3"
	} else {
		opts.IdentityEndpoint = iamEndpoint
	}
	opts.Username = os.Getenv("USER_NAME")
	opts.Password = os.Getenv("PASSWORD")
	opts.DomainName = os.Getenv("DOMAIN_NAME")
	// opts.TenantName = os.Getenv("TENANT_NAME")
	opts.AllowReauth = true
	return opts
}

//ConvertClient convert cluster to k8s client
func ConvertClient(ctx context.Context, cid string) (kubernetes.Interface, error) {
	cluster, err := client.ResourceV1beta1().Clusters().Get(cid, metav1.GetOptions{})
	if err != nil {
		log.Errorln(err)
		return nil, err
	}
	config := &rest.Config{
		Username: cluster.Spec.Auth.KubeUser,
		Password: cluster.Spec.Auth.KubePassword,
		Host:     fmt.Sprintf("https://%s:%s", cluster.Spec.Auth.EndpointIP, cluster.Spec.Auth.EndpointPort),
		TLSClientConfig: rest.TLSClientConfig{
			Insecure: true,
		},
	}
	if cluster.Spec.Auth.KubeConfig != nil {
		config, err = clientcmd.NewDefaultClientConfig(*cluster.Spec.Auth.KubeConfig, &clientcmd.ConfigOverrides{}).ClientConfig()
		if err != nil {
			log.Errorln(err)
			return nil, err
		}
	}
	newClient, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Errorln(err)
		return nil, err
	}
	return newClient, nil
}

// GetInitClient return default kubenetes client
func GetInitClient() kubernetes.Interface {
	return client
}
