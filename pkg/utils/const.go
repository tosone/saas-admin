package utils

const (
	ClusterProductIDAnno = "paper.people.com.cn/productid"

	// SaasActionCreate respresent a saas notify action
	SaasActionCreate = "createInstance"
	// SaasActionUpdate ..
	SaasActionUpdate = "updateInstance"
	// SaasActionDelete ..
	SaasActionDelete = "releaseInstance"

	// SaasActionExpire ..
	SaasActionExpire = "expiredInstance"
	// SaasActionRenew ..
	SaasActionRenew  = "renewInstance"
)
