package sender

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"hash/fnv"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"strconv"
	"time"

	core_v1 "k8s.io/api/core/v1"

	"github.com/caicloud/nirvana/errors"
	"github.com/caicloud/nirvana/log"

	"gitlab.com/tosone/saas-admin/pkg/types/saasv1"
)

// CreateUser create user with default password
func CreateUser(endpoint, name, email, defaultPasswd string) error {
	req := struct {
		UserName string `json:"username"`
		NickName string `json:"nickname"`
		Email    string `json:"email"`
		Password string `json:"password"`
	}{
		UserName: name,
		Email:    fmt.Sprintf("%s@people.cn", name),
		NickName: name,
		Password: defaultPasswd,
	}
	reqData, err := json.Marshal(req)
	if err != nil {
		return err
	}
	log.Infof("38: %+v\n", req)
	urlPath := fmt.Sprintf("%s/api/v2/users", endpoint)
	resp, err := http.Post(urlPath, "application/json", bytes.NewBuffer(reqData))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode/100 == 2 {
		return nil
	}
	if resp.StatusCode == http.StatusConflict {
		err = errors.Conflict.Build("ReasonRequest", "conflict: ${resource}").Error("email conflict")
		return err
	}
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Infof("53: %+v\n", respData)
	return fmt.Errorf("%s", string(respData))
}

// CreateTenant create a tenant in clever and return tenantId
func CreateTenant(endpoint, ownerName string) (string, error) {
	type Member struct {
		Name string `json:"name"`
		Role string `json:"role"`
	}
	log.Infof("65: %+v\n", ownerName)
	var fnvHash = fnv.New32()
	time.Now().Unix()
	if _, err := fnvHash.Write([]byte(strconv.FormatInt(time.Now().Unix(), 10))); err != nil {
		return "", err
	}
	var tenantName = hex.EncodeToString(fnvHash.Sum(nil)) // generate a hex string as tenant name
	req := struct {
		Name    string   `json:"name"`
		Members []Member `json:"members"`
	}{
		Name:    tenantName,
		Members: []Member{{Name: ownerName, Role: "owner"}},
	}
	reqData, err := json.Marshal(req)
	if err != nil {
		return "", err
	}
	urlPath := fmt.Sprintf("%s/api/v2/tenants", endpoint)
	resp, err := http.Post(urlPath, "application/json", bytes.NewBuffer(reqData))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if resp.StatusCode == http.StatusConflict {
		tenantID, err := getTenantID(endpoint, tenantName, ownerName)
		if err != nil {
			return "", err
		}
		return tenantID, nil
	} else if resp.StatusCode/100 != 2 {
		return "", fmt.Errorf("%s", string(respData))
	}

	teantID := struct {
		ID string `json:"id"`
	}{}
	err = json.Unmarshal(respData, &teantID)
	if err != nil {
		return "", err
	}
	return teantID.ID, nil
}

// AllocateTenantQuota allocate resource quota to a tenant
func AllocateTenantQuota(endpoint, tenantName string, productInfo saasv1.SaasTenantProductInfo) error {
	defaultSc := os.Getenv("DEFAULT_STORAGECLASS")
	if len(defaultSc) == 0 {
		defaultSc = "glusterfs-20191224103434-290552.storageclass.storage.k8s.io"
	}
	req := struct {
		Metadata struct {
			Name string `json:"name"`
		} `json:"metadata"`
		Spec struct {
			Quota map[core_v1.ResourceName]string `json:"quota"`
		} `json:"spec"`
	}{}
	req.Metadata.Name = tenantName
	req.Spec.Quota = map[core_v1.ResourceName]string{
		core_v1.ResourceRequestsCPU:    fmt.Sprintf("%d", productInfo.CPURequest),
		core_v1.ResourceLimitsCPU:      fmt.Sprintf("%d", productInfo.CPULimit),
		core_v1.ResourceRequestsMemory: fmt.Sprintf("%dGi", productInfo.MemoryRequest),
		core_v1.ResourceLimitsMemory:   fmt.Sprintf("%dGi", productInfo.MemoryLimit),
		core_v1.ResourceName(defaultSc + "/" + string(core_v1.ResourceRequestsStorage)):        fmt.Sprintf("%dGi", productInfo.StorageSize),
		core_v1.ResourceName(defaultSc + "/" + string(core_v1.ResourcePersistentVolumeClaims)): fmt.Sprintf("%d", productInfo.StorageNum),
		core_v1.ResourceName("requests.nvidia.com/gpu"):                                        fmt.Sprintf("%d", productInfo.GpuNum),
	}
	log.Infof("138:  %+v\n", req)
	reqData, err := json.Marshal(req)
	if err != nil {
		return err
	}
	urlPath := fmt.Sprintf("%s/apis/tenant.caicloud.io/v1alpha1/clusters/compass-stack/tenants", endpoint)
	resp, err := http.Post(urlPath, "application/json", bytes.NewBuffer(reqData))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	respData, err := ioutil.ReadAll(resp.Body)
	fmt.Println("154: ", string(respData))
	if err != nil {
		return err
	}
	if resp.StatusCode/100 == 2 ||
		resp.StatusCode == http.StatusConflict {
		return nil
	}

	return fmt.Errorf("%s", string(respData))
}

func getTenantID(endpoint string, alias, owner string) (string, error) {
	value := url.Values{}
	value.Set("name", alias)
	value.Set("user", owner)
	urlPath := fmt.Sprintf("%s/api/v2/tenants?%s", endpoint, value.Encode())
	resp, err := http.Get(urlPath)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}
	if resp.StatusCode/100 != 2 {
		return "", fmt.Errorf("%s", string(respData))
	}
	teantID := struct {
		Items []struct {
			ID string `json:"id"`
		} `json:"items"`
	}{}
	err = json.Unmarshal(respData, &teantID)
	if err != nil {
		return "", err
	}
	if len(teantID.Items) != 1 {
		return "", fmt.Errorf("%s", string(respData))
	}
	return teantID.Items[0].ID, nil
}

// UpdateTenantQuota update resource quota
func UpdateTenantQuota(endpoint, tenantID string, productInfo saasv1.SaasTenantProductInfo) error {
	defaultSc := os.Getenv("DEFAULT_STORAGECLASS")
	if len(defaultSc) == 0 {
		defaultSc = "glusterfs-20190802171751-eea369.storageclass.storage.k8s.io"
	}
	req := struct {
		Metadata struct {
			Name string `json:"name"`
		} `json:"metadata"`
		Spec struct {
			Quota map[core_v1.ResourceName]string `json:"quota"`
		} `json:"spec"`
	}{}
	req.Metadata.Name = tenantID
	req.Spec.Quota = map[core_v1.ResourceName]string{
		core_v1.ResourceRequestsCPU:    fmt.Sprintf("%d", productInfo.CPURequest),
		core_v1.ResourceLimitsCPU:      fmt.Sprintf("%d", productInfo.CPULimit),
		core_v1.ResourceRequestsMemory: fmt.Sprintf("%dGi", productInfo.MemoryRequest),
		core_v1.ResourceLimitsMemory:   fmt.Sprintf("%dGi", productInfo.MemoryLimit),
		core_v1.ResourceName(defaultSc + "/" + string(core_v1.ResourceRequestsStorage)):        fmt.Sprintf("%dGi", productInfo.StorageSize),
		core_v1.ResourceName(defaultSc + "/" + string(core_v1.ResourcePersistentVolumeClaims)): fmt.Sprintf("%d", productInfo.StorageNum),
		core_v1.ResourceName("requests.nvidia.com/gpu"):                                        fmt.Sprintf("%d", productInfo.GpuNum),
	}
	reqData, err := json.Marshal(req)
	if err != nil {
		return err
	}

	urlPath := fmt.Sprintf("%s/apis/tenant.caicloud.io/v1alpha1/clusters/compass-stack/tenants/%s", endpoint, tenantID)
	request, err := http.NewRequest(http.MethodPut, urlPath, bytes.NewBuffer(reqData))
	if err != nil {
		return err
	}
	request.Header.Set("Content-Type", "application/json")

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode/100 == 2 {
		return nil
	}
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return fmt.Errorf("%s", string(respData))
}

// DeleteTenantQuota delete tenant from cluster
func DeleteTenantQuota(endpoint, tenantID string) error {
	urlPath := fmt.Sprintf("%s/apis/tenant.caicloud.io/v1alpha1/clusters/compass-stack/tenants/%s", endpoint, tenantID)
	request, err := http.NewRequest(http.MethodDelete, urlPath, nil)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode/100 == 2 ||
		resp.StatusCode == http.StatusNotFound {
		return nil
	}
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return fmt.Errorf("%s", string(respData))
}

// DeleteTenant delete tenant from mongo
func DeleteTenant(endpoint, tenantID string) error {
	urlPath := fmt.Sprintf("%s/api/v2/tenants/%s", endpoint, tenantID)
	request, err := http.NewRequest(http.MethodDelete, urlPath, nil)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(request)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode/100 == 2 ||
		resp.StatusCode == http.StatusNotFound {
		return nil
	}
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	return fmt.Errorf("%s", string(respData))
}
