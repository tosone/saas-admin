package sender

import (
	"bytes"
	"context"
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"

	"github.com/tidwall/sjson"

	"github.com/caicloud/nirvana/log"

	"github.com/tidwall/gjson"
)

const (
	URLCtx      = "urlCtx"
	SupplierKey = "supplierKey"
)

// Send send post to web server
func Send(ctx context.Context, data []byte, out interface{}) error {
	path, ok := ctx.Value(URLCtx).(string)
	if !ok {
		return fmt.Errorf("can't get request path")
	}
	supplierKey, ok := ctx.Value(SupplierKey).(string)
	if !ok {
		return fmt.Errorf("can't get supplier key")
	}
	needSign := GetSignData(data, supplierKey)
	if needSign == "" {
		return fmt.Errorf("generate sign data error")
	}
	signature := md5.Sum([]byte(needSign))
	sjson.SetBytes(data, "sign", fmt.Sprintf("%x", signature))
	log.Infof("requestPath:%s, signData:%s, signature:%x\n", path, needSign, signature)
	resp, err := http.Post(path, "application/json", bytes.NewReader(data))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	// TODO: verify signature
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Infof("responseCode:%d,respdata:%s\n", resp.StatusCode, string(respData))
	err = gjson.Unmarshal(respData, &out)
	return err
}

// GetSignData return string data for sign
func GetSignData(v []byte, key string) string {
	pr := gjson.ParseBytes(v)
	if !pr.IsObject() {
		log.Errorln("input value must be a object")
		return ""
	}
	// sort top level field
	var mapdata []value
	pr.ForEach(func(key, v gjson.Result) bool {
		mapdata = append(mapdata, value{key: key.Str, value: sortJSONData(v)})
		return true
	})
	sort.Slice(mapdata, func(i, j int) bool {
		return mapdata[i].key < mapdata[j].key
	})
	signData := ""
	for _, d := range mapdata {
		signData += fmt.Sprintf("%s=%s&", d.key, d.value)
	}
	signData += "key=" + key
	return signData
}

type value struct {
	key   string
	value string
}

func sortJSONData(v gjson.Result) string {
	s := v.Raw
	if v.IsObject() {
		var mapdata []value
		v.ForEach(func(key, v gjson.Result) bool {
			vv := value{key: key.Str, value: sortJSONData(v)}
			mapdata = append(mapdata, vv)
			return true
		})
		sort.Slice(mapdata, func(i, j int) bool {
			return mapdata[i].key < mapdata[j].key
		})
		s = "{"
		for i, d := range mapdata {
			s += fmt.Sprintf(`"%s":%s`, d.key, d.value)
			if len(mapdata)-1 > i {
				s += ","
			}
		}
		s += "}"
	}
	if v.IsArray() {
		var mapdata []string
		for _, r := range v.Array() {
			mapdata = append(mapdata, sortJSONData(r))
		}
		s = "["
		for i, d := range mapdata {
			s += fmt.Sprintf(`%s`, d)
			if len(mapdata)-1 > i {
				s += ","
			}
		}
		s += "]"
	}
	return s
}
