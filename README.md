# peoplepaper

人民日报项目

- saas-admin 暴露给 05 包的 API 接口，主要用于 Clever 的 SaaS 方式接入使用。
适配 Compass 的 2.8 版本。
  
- tp-saas-admin 暴露给 05 包的 API 接口，主要用于 Compass 的 SaaS 方式接入使用。
适配 Compass 的 2.9 版本。

本地测试的时候需要将 `postman.json` 导入到 Postman 中，然后修改代码中关于 timestamp 的部分(tphandler 68 行)，代码中校验请求时间戳不得小于服务器时间 10min。

<!-- Write one paragraph of this project description here -->

<!-- ## Getting Started -->

<!-- ### Prerequisites -->

<!-- Describe packages, tools and everything we needed here -->

<!-- ### Building -->

<!-- Describe how to build this project -->

<!-- ### Running -->

<!-- Describe how to run this project -->

<!-- ## Versioning -->

<!-- Place versions of this project and write comments for every version -->

<!-- ## Contributing -->

<!-- Tell others how to contribute this project -->

<!-- ## Authors  -->

<!-- Put authors here -->

<!-- ## License -->

<!-- A link to license file -->

